BOOTSTRAP 5 
-----

### Installation des dépendances 
```
$ yarn install
```

### Générer le site 

En DEV :
```
$ yarn run dev
```

En PROD :
```
$ yarn run build
```

### Documentation 

- Bootstrap V5 : https://v5.getbootstrap.com/
