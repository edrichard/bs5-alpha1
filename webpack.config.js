const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

// on récupère la valeur de NODE_ENV
const env = process.env.NODE_ENV;

module.exports = {
    mode: env || 'development', // on définit le mode en fonction de la valeur de NODE_ENV
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[chunkhash].js'
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                include: path.resolve(__dirname),
                loader: 'html-loader',
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.sc|ass$/,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    {
                        loader: "css-loader",
                        options: {
                            importLoaders: 1
                        }
                    },
                    {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true // il est indispensable d'activer les sourcemaps pour que postcss fonctionne correctement
                        }
                    }
                ]
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "main.[contenthash].css",
            chunkFilename: "[id].css"
        }),
        new CleanWebpackPlugin(), // supprime tous les fichiers du répertoire dist sans pour autant supprimer ce dossier
        new HtmlWebpackPlugin({
            hash: true,
            template: './src/index.html',
            filename: 'index.html'
        })
    ],
};
